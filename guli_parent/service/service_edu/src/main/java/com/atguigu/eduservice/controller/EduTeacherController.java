package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.vo.TeacherQuery;
import com.atguigu.eduservice.service.EduTeacherService;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author tll
 * @since 2022-02-14
 */
@Api(tags = "讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
@CrossOrigin
public class EduTeacherController {

    @Autowired
    private EduTeacherService eduTeacherService;

    //查询所有讲师
    @ApiOperation(value = "查询所有讲师列表")
    @GetMapping("/findAllTeacher")
    public R findAllTeacher() {
        List<EduTeacher> list = eduTeacherService.list(null);
        return R.ok().data("items", list);
    }

    //根据id进行逻辑删除
    @ApiOperation(value = "根据id逻辑删除讲师")
    @DeleteMapping("{id}")
    public R removeById(@ApiParam(name = "id", value = "讲师ID", required = true)
                        @PathVariable String id) {
        boolean flag = eduTeacherService.removeById(id);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }

    }

    /**
     * 分页查询讲师列表
     *
     * @return
     */
    @GetMapping("{page}/{limit}")
    public R pageList(@ApiParam(name = "page", value = "当前页码", required = true)
                      @PathVariable Long page,

                      @ApiParam(name = "limit", value = "每页记录数", required = true)
                      @PathVariable Long limit) {
        Page<EduTeacher> pageParam = new Page<>(page, limit);
        //分页查询后会将返回信息封装至pageParam中
        eduTeacherService.page(pageParam, null);
        //返回讲师列表
        List<EduTeacher> records = pageParam.getRecords();
        long total = pageParam.getTotal();
        //链式编程
        return R.ok().data("total", total).data("rows", records);
    }


    /**
     * 条件查询带分页方法
     *
     * @param current
     * @param limit
     * @return
     */
    @PostMapping("pageTeacherCondition/{current}/{limit}")
    public R pageTeacherCondition(@ApiParam(name = "current", value = "当前页码", required = true)
                                  @PathVariable long current,

                                  @ApiParam(name = "limit", value = "每页记录数", required = true)
                                  @PathVariable long limit,

                                  @ApiParam(name = "teacherQuery", value = "查询对象", required = false)
                                  @RequestBody(required = false) TeacherQuery teacherQuery) {

        Page<EduTeacher> pageParam = new Page<>(current, limit);
        //分页查询后会将返回信息封装至pageParam中
        eduTeacherService.pageQuery(pageParam, teacherQuery);
        //返回讲师列表
        List<EduTeacher> records = pageParam.getRecords();
        long total = pageParam.getTotal();
        //链式编程
        return R.ok().data("total", total).data("rows", records);
    }


    /**
     * 新增讲师信息
     *
     * @param eduTeacher
     * @return
     */
    @ApiOperation("新增讲师")
    @PostMapping("addTeacher")
    public R save(@RequestBody EduTeacher eduTeacher) {
        boolean flag = eduTeacherService.save(eduTeacher);
        if (flag) {
            return R.ok().message("讲师信息保存成功");
        } else {
            return R.error().message("讲师信息保存失败");
        }
    }

    @ApiOperation("根据id查询讲师")
    @GetMapping("getTeacher/{id}")
    public R getById(@ApiParam(name = "id", value = "讲师id", required = true)
                     @PathVariable String id) {
//        try {
//            int i = 10/0;
//        } catch (Exception e) {
//            throw new GuliException(20001,"分母等于零");
//        }

        EduTeacher eduTeacher = eduTeacherService.getById(id);
        if (eduTeacher != null) {
            return R.ok().data("teacher",eduTeacher);
        } else {
            return R.error().message("查询失败");
        }
    }

    @ApiOperation("根据ID修改讲师")
    @PostMapping("updateTeacher")
    public R updateById(@RequestBody EduTeacher eduTeacher) {
        boolean flag = eduTeacherService.updateById(eduTeacher);
        if (flag) {
            return R.ok().message("根据ID修改讲师成功");
        } else {
            return R.error().message("根据ID修改讲师失败");
        }
    }


}

