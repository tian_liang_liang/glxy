package com.atguigu.eduservice.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.excel.SubjectData;
import com.atguigu.eduservice.service.EduSubjectService;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;


/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/22 15:27
 */
public class SubjectExcelListener extends AnalysisEventListener<SubjectData> {

    //因为SubjectExcelListener不能交给Spring管理 listener在spring之前执行 需要自己new,不能注入其他对象
    private EduSubjectService eduSubjectService;

    //无参构造
    public SubjectExcelListener() {
    }

    //有参构造
    public SubjectExcelListener(EduSubjectService eduSubjectService) {
        this.eduSubjectService = eduSubjectService;
    }

    //读取excel中的内容,一行一行的读
    @Override
    public void invoke(SubjectData subjectData, AnalysisContext analysisContext) {
        if (subjectData == null) {
            throw new GuliException(20001, "文件数据为空");
        }
        //一行一行读取,每次获取有两个值,第一个值为一级分类,第二个值为二级分类
        //判断一级分类是否重复
        System.out.println("hhhhhhhhhhhhhhh"+subjectData.toString());
        EduSubject oneSubject = this.existOneSubject(eduSubjectService, subjectData.getOneSubjectName());
        if (oneSubject == null) {
            //一级标题不存在,进行添加
            oneSubject = new EduSubject();
            oneSubject.setTitle(subjectData.getOneSubjectName());
            oneSubject.setParentId("0");
            eduSubjectService.save(oneSubject);
        }
        String parentId = oneSubject.getId();
        //判断二级标签是否存在
        EduSubject twoSubject = this.existTwoSubject(eduSubjectService, subjectData.getTwoSubjectName(), parentId);
        if (twoSubject ==null){
            twoSubject = new EduSubject();
            twoSubject.setTitle(subjectData.getTwoSubjectName());
            twoSubject.setParentId(parentId);
            eduSubjectService.save(twoSubject);
        }


    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

    //判断一级分类是否重复
    private EduSubject existOneSubject(EduSubjectService eduSubjectService, String oneSubjectName) {
        //通过QuerryMapper来进行查询
        QueryWrapper<EduSubject> queryWrapper = new QueryWrapper<>();
        //添加查询条件 一级标题名称对应
        queryWrapper.eq("title", oneSubjectName);
        queryWrapper.eq("parent_id", "0");
        EduSubject one = eduSubjectService.getOne(queryWrapper);
        return one;
    }

    //判断二级标签是否存在
    private EduSubject existTwoSubject(EduSubjectService eduSubjectService, String subjectName, String parentId) {
        QueryWrapper<EduSubject> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", parentId);
        queryWrapper.eq("title", subjectName);
        EduSubject twoSubject = eduSubjectService.getOne(queryWrapper);
        return twoSubject;
    }
}
