package com.atguigu.eduservice.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/22 15:19
 * 课程表格数据类型
 */
@Data
public class SubjectData {

    @ExcelProperty(index = 0)
    private String OneSubjectName;

    @ExcelProperty(index = 1)
    private String TwoSubjectName;
}
