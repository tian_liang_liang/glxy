package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.vo.TeacherQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author tll
 * @since 2022-02-14
 */
public interface EduTeacherService extends IService<EduTeacher> {

    //条件分页查询
    void pageQuery(Page<EduTeacher> pageParam, TeacherQuery queryWrapper);
}
