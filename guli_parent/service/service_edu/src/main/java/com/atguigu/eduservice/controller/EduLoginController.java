package com.atguigu.eduservice.controller;

import com.atguigu.commonutils.R;
import org.springframework.web.bind.annotation.*;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/17 21:31
 * 解决跨域问题@CrossOrigin
 */
@CrossOrigin
@RestController
@RequestMapping("/eduservice/user")
public class EduLoginController {

    //用户登录接口
    @PostMapping("login")
    public R login(){
        return R.ok().data("token","token");
    }

    //用户个人信息
    @GetMapping("info")
    public R info(){
        return R.ok().data("roles","[admin]").data("name","tll").data("avatar","https://cdn.apifox.cn/www/screenshot/dark-apifox-api-case-1.png");
    }
}
