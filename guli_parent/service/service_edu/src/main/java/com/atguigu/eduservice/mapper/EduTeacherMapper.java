package com.atguigu.eduservice.mapper;

import com.atguigu.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author tll
 * @since 2022-02-14
 */
@Repository
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
