package com.atguigu.excel;

import com.alibaba.excel.EasyExcel;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/24 14:32
 */
public class ReadExcel {
    public static void main(String[] args) {
        String filePath = "D:\\01.xlsx";
        EasyExcel.read(filePath,DemoData.class,new ExcelListener()).sheet().doRead();
    }
}
