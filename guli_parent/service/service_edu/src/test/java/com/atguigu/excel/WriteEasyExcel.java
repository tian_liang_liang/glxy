package com.atguigu.excel;

import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/24 14:13
 * 测试EasyExcel写方法
 */
public class WriteEasyExcel {

    public static void main(String[] args) {
        String filePath = "D:\\01.xlsx";
        EasyExcel.write(filePath,DemoData.class).sheet("学生列表").doWrite(getData());
    }


    public static List<DemoData> getData(){
        List<DemoData> list = new ArrayList<>();
        String chars = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < 10; i++) {
            DemoData data = new DemoData();
            data.setSno(i);
            data.setSname(String.valueOf(chars.charAt((int)(Math.random() * 26))));
            list.add(data);
        }
        return list;
    }
}
