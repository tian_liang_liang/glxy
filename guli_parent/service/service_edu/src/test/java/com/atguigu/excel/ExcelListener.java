package com.atguigu.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Map;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/24 14:29
 */
public class ExcelListener extends AnalysisEventListener<DemoData> {
    //一行一行读取excel中的内容
    @Override
    public void invoke(DemoData demoData, AnalysisContext analysisContext) {
        System.out.println(demoData+"HHHHHHHHHH");
    }


    //读取表头信息
    @Override
    public void invokeHeadMap(Map<Integer,String> haedMap,AnalysisContext context){
        System.out.println("表头信息"+haedMap);
    }

    //读取完成之后执行的方法
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
