package com.atguigu.oss.controller;

import com.atguigu.commonutils.R;
import com.atguigu.oss.service.OssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/21 20:31
 */
@Api(tags = "阿里云文件管理")
@CrossOrigin
@RestController
@RequestMapping("/eduoss/fileoss")
public class OssController {

    @Autowired
    private OssService ossService;

    //上传头像接口
    @ApiOperation(value = "头像上传")
    @PostMapping()
    public R uploadAvatar(@ApiParam(name = "file", value = "文件", required = true)
                                  MultipartFile file) {
        String url = ossService.uploadAvatar(file);
        return R.ok().data("url",url);
    }
}
