package com.atguigu.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/21 20:31
 */
public interface OssService {

    //头像上传
    String uploadAvatar(MultipartFile multipartFile);
}
