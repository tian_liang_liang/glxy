package com.atguigu.servicebase.exceptionhandler;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/15 14:53
 * 集成RuntimeException 声明是一个异常类
 */
@Data
@AllArgsConstructor     //生成全参构造方法
@NoArgsConstructor      //生成无参构造方法
public class GuliException extends RuntimeException{
    //状态码
    @ApiModelProperty(value ="状态码")
    private Integer code;
    //异常信息
    @ApiModelProperty(value = "异常信息")
    private String msg;

    @Override
    public String toString() {
        return "GuliException{" +
        "msg=" + this.getMessage() +
        ",code=" + code +
        '}';
    }
}
