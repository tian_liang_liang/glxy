package com.atguigu.servicebase.exceptionhandler;

import com.atguigu.commonutils.R;
import com.atguigu.utils.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/15 14:36
 * 统一异常处理类
 * @ControllerAdvice 异常处理接收器
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    //添加异常处理方法
    @ExceptionHandler(GuliException.class)
    @ResponseBody
    public R error(GuliException e){
        log.error(ExceptionUtil.getMessage(e));
        return R.error().message(e.getMsg()).code(e.getCode());
    }

    //执行什么异常出现该方法
    @ExceptionHandler(Exception.class)
    @ResponseBody   //返回rest风格数据类型
    public R error(Exception e){
        return R.error().message(e.getMessage());
    }


}
