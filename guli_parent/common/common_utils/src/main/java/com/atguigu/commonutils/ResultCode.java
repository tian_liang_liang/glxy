package com.atguigu.commonutils;

import io.swagger.models.auth.In;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/2/14 15:39
 * 接口自定义返回码
 */
public interface ResultCode {

    public static Integer SUCCESS = 20000;

    public static Integer ERROR = 20001;

}
